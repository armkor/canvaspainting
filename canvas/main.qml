import QtQuick 2.12
import QtQuick.Controls 1.6
import QtLocation 5.6
import QtPositioning 5.5

ApplicationWindow {
    visible: true
    width: 1200
    height: 1000

    property double startTime: 0

Canvas {
    id: root
    anchors.fill: parent
    onPaint: {

            console.time("Canvas started")
            var ctx = getContext("2d")
            ctx.lineWidth = 3
            ctx.globalAlpha = 0.3
            ctx.miterLimit = 0.1
            ctx.strokeStyle = "red"

            var coord_arr = lineModel.getList()

            startTime = new Date().getTime()

            for(var i = 0; i < coord_arr.length; i ++) {
                ctx.beginPath()
                ctx.moveTo(coord_arr[i].x, coord_arr[i].y);
                var j = 0
                while (j < 49)
                {
                    ctx.lineTo(coord_arr[i].x, coord_arr[i].y)
                    j ++;
                    i ++;
                }

                ctx.stroke()
            }

            console.log("Time of prepairing in s : ", (new Date().getTime() - startTime )/ 1000.0 )

        }

    onPainted: {
        console.timeEnd("Canvas ended")
        console.log("//////Time of painting in s : ", (new Date().getTime() - startTime )/ 1000.0 )
    }
}
}
