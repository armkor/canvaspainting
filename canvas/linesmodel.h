#pragma once
#include <QObject>
#include <QPoint>
#include <QVariantList>


class LinesModel : public QObject
{
	Q_OBJECT
public:
	explicit LinesModel(int linesSize = 10, int sizeX = 1200, int sizeY = 1000, QObject *parent = nullptr);

	Q_INVOKABLE QVariant getList();
private:
	QVariantList lines;
};
